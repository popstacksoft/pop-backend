<div class="row small">
    <div class="col-md-12">
        <div class="bs-component">
            <div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Pop Backend Login</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="<@ofbizUrl>login</@ofbizUrl>">
                        <input type="hidden" name="redirect_uri" value="${parameters.redirect_uri?if_exists}" id="OAuth2_redirect_uri">
                        <fieldset>
                            <div class="form-group">
                                <label for="username" class="col-md-2 control-label">Username</label>
                                <div class="col-md-10">
                                    <input name="USERNAME" type="text" class="form-control" id="username" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-md-2 control-label">Password</label>
                                <div class="col-md-10">
                                    <input name="PASSWORD" type="password" class="form-control" id="username" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-2">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Login</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>