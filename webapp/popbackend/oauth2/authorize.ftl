<div class="row small">
    <div class="col-md-12">
        <div class="bs-component">
            <div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Pop Backend OAuth2</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="<@ofbizUrl>authorize</@ofbizUrl>" id="OAuth2" class="basic-form" name="OAuth2">
                        <fieldset>
                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-2">
                                    <button type="button" class="btn btn-default" onclick="window.location='<@ofbizUrl>logout</@ofbizUrl>'">Logout</button>
                                    <button type="submit" class="btn btn-primary">Authorize</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>