import java.util.Map;
import java.security.SecureRandom;

import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.HttpClient;
import org.apache.ofbiz.base.util.HttpClientException;
import org.apache.ofbiz.base.util.UtilDateTime;
import org.apache.ofbiz.base.util.UtilGenerics;
import org.apache.ofbiz.base.util.UtilHttp;
import org.apache.ofbiz.base.util.UtilProperties;
import org.apache.ofbiz.base.util.UtilValidate;
import org.apache.ofbiz.base.util.UtilXml;
import org.apache.ofbiz.base.crypto.HashCrypt;
import org.apache.ofbiz.common.login.LoginServices;
import org.apache.commons.codec.binary.Hex;

import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.util.EntityQuery;
import org.apache.ofbiz.entity.util.EntityUtilProperties;
import org.apache.ofbiz.service.ServiceUtil;


public Map popLoginApi() {
    Map results = ServiceUtil.returnSuccess();
    securityextUiLabels = UtilProperties.getResourceBundleMap("SecurityextUiLabels", locale);
    password = HashCrypt.cryptUTF8(LoginServices.getHashType(), null, parameters.password);
    userLoginResult = from("UserLogin").where("userLoginId", parameters.username).queryOne();
    if (!userLoginResult || "N".equals(userLoginResult.enabled)) {
        results = ServiceUtil.returnError(securityextUiLabels.get("loginservices.user_not_found"));
    } else {
        validPassword = HashCrypt.comparePassword(userLoginResult.get("currentPassword"), LoginServices.getHashType(), parameters.password);
        if (!validPassword) {
            results = ServiceUtil.returnError(securityextUiLabels.get("loginservices.password_incorrect"));
        } else {
            random = new SecureRandom();
            bytes = new byte[20];
            random.nextBytes(bytes);
            token = Hex.encodeHexString(bytes);
            visitId = delegator.getNextSeqId("Visit")
            visit = delegator.makeValue("Visit");
            visit.set("visitId", visitId);
            visit.set("userLoginId", userLoginResult.userLoginId);
            visit.set("partyId", userLoginResult.partyId);
            visit.set("initialRequest", "callServiceApi");
            visit.set("initialUserAgent", "popbackend");
            visit.set("cookie", token);
            visit.set("fromDate", UtilDateTime.nowTimestamp());
            visit.set("thruDate", new java.sql.Timestamp(UtilDateTime.nowTimestamp().getTime() + 3600 * 1000));
            visit.create();
            results.put("token", token);
        }
    }
    return results;
}
public Map popLogoutApi() {
    Map results = ServiceUtil.returnSuccess();
    visitList = from("Visit").where("cookie", parameters.token).filterByDate().queryList();
    for (visit in visitList) {
        visit.thruDate = UtilDateTime.nowTimestamp();
        visit.store();
    }
    return results;
}
public Map popCheckLoginApi() {
    Map result = ServiceUtil.returnSuccess();
    visitList = from("Visit").where("cookie", parameters.token).filterByDate().queryList();
    Debug.log("===visitList========: " + visitList);
    if (visitList) {
        result.isExpired = "N";
    } else {
        result.isExpired = "Y";
    }
    return result;
}