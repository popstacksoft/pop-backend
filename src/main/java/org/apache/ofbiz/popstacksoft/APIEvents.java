package org.apache.ofbiz.popstacksoft;

import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilDateTime;
import org.apache.ofbiz.base.util.UtilHttp;
import org.apache.ofbiz.base.util.UtilValidate;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.util.EntityQuery;
import org.apache.ofbiz.service.GenericServiceException;
import org.apache.ofbiz.service.LocalDispatcher;
import org.apache.ofbiz.service.ModelService;

public class APIEvents {
    public static final String module = APIEvents.class.getName();
    public static final String resource = "PopBackendUiLabels";

    public static String callServiceApi(HttpServletRequest request, HttpServletResponse response) {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        try {
            Map<String, Object> parameters = UtilHttp.getCombinedMap(request);
            String serviceName = (String) parameters.get("service");
            Debug.logInfo("Call API Service: " + serviceName, module);
            if (UtilValidate.isNotEmpty(serviceName)) {
                ModelService model = dispatcher.getDispatchContext().getModelService(serviceName);
                Map<String, Object> inMap = (Map<String, Object>) parameters.get("data");
                Map<String, Object> ctx = model.makeValid(inMap, ModelService.IN_PARAM);
                GenericValue visit = EntityQuery.use(delegator).from("Visit").where("cookie", request.getHeader("authorization")).filterByDate().queryFirst();
                if(UtilValidate.isNotEmpty(visit) && model.auth) {
                    visit.set("thruDate", new java.sql.Timestamp(UtilDateTime.nowTimestamp().getTime() + 3600 * 1000));
                    visit.store();
                    GenericValue userLogin = EntityQuery.use(delegator).from("UserLogin").where("userLoginId", visit.get("userLoginId")).queryOne();
                    ctx.put("userLogin", userLogin);
                }
                Map<String, Object> result = dispatcher.runSync(serviceName, ctx);
                if (ModelService.RESPOND_SUCCESS.equals(result.get(ModelService.RESPONSE_MESSAGE))) {
                    request.removeAttribute("data");
                }
                for (Entry<String, Object> entry : result.entrySet()) {
                    request.setAttribute(entry.getKey(), entry.getValue());
                }
            } else {
                request.setAttribute(ModelService.ERROR_MESSAGE, "No service found!");
                return ModelService.RESPOND_ERROR;
            }
        } catch (GenericServiceException e) {
            request.setAttribute(ModelService.ERROR_MESSAGE, e.getMessage());
            Debug.logError(e, e.getMessage(), module);
            return ModelService.RESPOND_ERROR;
        } catch (GenericEntityException e) {
            request.setAttribute(ModelService.ERROR_MESSAGE, e.getMessage());
            Debug.logError(e, module);
            return ModelService.RESPOND_ERROR;
        }
        return ModelService.RESPOND_SUCCESS;
    }

}
